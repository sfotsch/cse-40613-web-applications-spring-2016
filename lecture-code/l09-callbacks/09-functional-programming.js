/**
 * Created by jesus on 2/8/16.
 */
"use strict";

var myArr = [0, 1, 1, 2, 3, 5, 8, 13, 21];
//forEach applies a function to each element of an array.
myArr.forEach(function(x){console.log(Math.pow(x,3));});
// map applies a function to each element of an array,
// and stores the result in a new array
var myCubed = myArr.map(function(x){return Math.pow(x,3);});
console.log(myCubed);
// reduce applies a function to every 2 contiguous elements
// starting at an initial value
var sumSquares = myArr.map(function(x){return x*x;}).reduce(function (a,b) {return a+b;},0);
console.log(sumSquares);
// filter extracts elements out of an array that meet some condition
var even = myArr.filter(function(x) {return x%2 === 0;});
console.log(even)
var myMax = Math.max.apply(null,myArr);
var myMax2 = Math.max(...myCubed);
console.log(myMax,myMax2);

var arrays = [[1, 2, 3], [4, 5], [6]];
var flat = arrays.reduce(function(a,b){return a.concat(b);});
console.log(flat);

