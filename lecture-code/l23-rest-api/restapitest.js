/**
 * Created by jesus on 4/4/16.
 * Test Firebase REST API using restler
 */
"use strict";
var rest = require('restler');

var base = "https://myvideo-app.firebaseio.com/";

rest.get(base + 'cars.json')
    .on('success', function (data, res) {
        console.log("read:", data);
        //console.log("response object:", res);
    })
    .on('fail', function (data, res) {
        console.log("failed:", data);
        //console.log("failed response object", res);
    })
    .on('error', function (err, res) {
        console.log("error:", err);
        //console.log("error response object", res);
    });

var sp85d = JSON.stringify({sp85d: "Tesla Model S P85D"});
rest.patch(base + 'names.json', {data: sp85d})
    .on('success', function (data, res) {
        console.log("wrote:", data);
    })
    .on('fail', function (data, res) {
        console.log("failed:", data);
    })
    .on('error', function (err, res) {
        console.log("error:", err);
    });

