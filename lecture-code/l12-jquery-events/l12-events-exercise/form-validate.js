"use strict";

var phonePattern, emailPattern;

// Phone numbers should match this pattern
phonePattern = /^\d{3}\-\d{3}\-\d{4}$/;

// Email addresses should match this pattern
emailPattern = /^\w+@\w+\.\w+$/;

$(function() {
    // Write your code here
    // 1. Use JQuery to select each form element in contact.html
    // 2. Use the JQuery selection.on("blur", callback) function to attach a callback to the selected element
    // 3. Inside the callback, validate the contents of the element
});
